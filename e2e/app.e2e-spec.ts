import { HrsolutionsParkingFrontPage } from './app.po';

describe('hrsolutions-parking-front App', () => {
  let page: HrsolutionsParkingFrontPage;

  beforeEach(() => {
    page = new HrsolutionsParkingFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
